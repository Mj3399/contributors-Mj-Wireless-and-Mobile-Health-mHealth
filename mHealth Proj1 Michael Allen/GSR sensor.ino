const int GSR = A0;  //Using A0 pin which is connected to the grove
int sensorValue = 0;
int gsr_average = 0;
void setup() {

  Serial.begin(9600);  //Setting the baud rate for serial communication

}
void loop() {

  long sum = 0;

  for (int i = 0; i < 10; i++)  //Average the 10 measurements to remove the glitch
  {
    sensorValue = analogRead(GSR);
    sum += sensorValue;
    delay(5);  //Sampling data with a delay for stable signal
  }

  gsr_average = sum / 10;       //Taking the average of the last 10 readings
  Serial.println(gsr_average);  //Printing the result via serial port
}